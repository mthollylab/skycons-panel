'use strict';

System.register(['./skycons_ctrl'], function (_export, _context) {
  "use strict";

  var SkyconsCtrl;
  return {
    setters: [function (_skycons_ctrl) {
      SkyconsCtrl = _skycons_ctrl.SkyconsCtrl;
    }],
    execute: function () {
      _export('PanelCtrl', SkyconsCtrl);
    }
  };
});
//# sourceMappingURL=module.js.map
