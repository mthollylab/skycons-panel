'use strict';

System.register(['app/plugins/sdk', 'lodash', 'app/core/time_series2'], function (_export, _context) {
  "use strict";

  var MetricsPanelCtrl, _, TimeSeries, _createClass, panelDefaults, SkyconsCtrl;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_appPluginsSdk) {
      MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
    }, function (_lodash) {
      _ = _lodash.default;
    }, function (_appCoreTime_series) {
      TimeSeries = _appCoreTime_series.default;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      panelDefaults = {
        iconWidth: 100
      };

      _export('SkyconsCtrl', SkyconsCtrl = function (_MetricsPanelCtrl) {
        _inherits(SkyconsCtrl, _MetricsPanelCtrl);

        function SkyconsCtrl($scope, $injector, alertSrv) {
          _classCallCheck(this, SkyconsCtrl);

          var _this = _possibleConstructorReturn(this, (SkyconsCtrl.__proto__ || Object.getPrototypeOf(SkyconsCtrl)).call(this, $scope, $injector));

          _.defaults(_this.panel, panelDefaults);

          _this.events.on('init-edit-mode', _this.onInitEditMode.bind(_this));
          _this.events.on('data-received', _this.onDataReceived.bind(_this));
          _this.events.on('data-snapshot-load', _this.onDataReceived.bind(_this));
          _this.events.on('data-error', _this.onDataError.bind(_this));
          return _this;
        }

        _createClass(SkyconsCtrl, [{
          key: 'onInitEditMode',
          value: function onInitEditMode() {}
        }, {
          key: 'onDataReceived',
          value: function onDataReceived(dataList) {
            this.series = dataList.map(this.seriesHandler.bind(this));
            this.data = this.series[0].datapoints[0][0];
            this.render(this.data);
          }
        }, {
          key: 'seriesHandler',
          value: function seriesHandler(seriesData) {
            var series = new TimeSeries({
              datapoints: seriesData.datapoints,
              alias: seriesData.target
            });

            series.flotpairs = series.getFlotPairs(this.panel.nullPointMode);
            return series;
          }
        }, {
          key: 'onDataError',
          value: function onDataError(err) {
            this.onDataReceived([]);
          }
        }, {
          key: 'link',
          value: function link(scope, elem, attrs, ctrl) {
            elem = elem.find('.skycons-panel');

            function setElementHeight() {
              elem.css('height', ctrl.height + 'px');
            }

            function render() {
              setElementHeight();

              var icon = ctrl.data;
              if (!icon) {
                elem.html('');
                return;
              }

              var width = elem.width();
              var height = ctrl.height;

              // Compute the dimensions for the weather icon
              var dim;
              if (ctrl.iconWidth) {
                dim = ctrl.iconWidth;
              } else {
                dim = Math.floor(Math.min(width, height) * .75);
              }

              // Find the position for the canvas inside the <div>
              var canvasTop = Math.floor(height / 2 - dim / 2);
              var canvasLeft = Math.floor(width / 2 - dim / 2);

              var body = '';
              body += '<div width="' + width + '" height="' + height + '">';
              body += '  <canvas id="icon1" width="' + dim + '" height="' + dim + '" style="position: absolute; margin-top: ' + canvasTop + 'px; margin-left: ' + canvasLeft + 'px;"></canvas>';
              body += '  <script src="public/plugins/skycons-panel/scripts/skycons.js"></script>';
              body += '  <script>';
              body += '    var skycons = new Skycons({monochrome: false});';
              body += '    skycons.add("icon1", "' + icon + '");';
              body += '    skycons.play();';
              body += '  </script>';
              body += "</div>";

              elem.html(body);
            }

            this.events.on('render', function () {
              render();
              ctrl.renderingCompleted();
            });
          }
        }]);

        return SkyconsCtrl;
      }(MetricsPanelCtrl));

      _export('SkyconsCtrl', SkyconsCtrl);

      SkyconsCtrl.templateUrl = 'module.html';
    }
  };
});
//# sourceMappingURL=skycons_ctrl.js.map
