import {MetricsPanelCtrl} from 'app/plugins/sdk';
import _ from 'lodash';
import TimeSeries from 'app/core/time_series2';

const panelDefaults = {
    iconWidth: 100
};

export class SkyconsCtrl extends MetricsPanelCtrl {

  constructor($scope, $injector, alertSrv) {
    super($scope, $injector);
    _.defaults(this.panel, panelDefaults);

    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.events.on('data-received', this.onDataReceived.bind(this));
    this.events.on('data-snapshot-load', this.onDataReceived.bind(this));
    this.events.on('data-error', this.onDataError.bind(this));
  }

  onInitEditMode() {
  }

  onDataReceived(dataList) {
    this.series = dataList.map(this.seriesHandler.bind(this));
    this.data = this.series[0].datapoints[0][0];
    this.render(this.data);
  }

  seriesHandler(seriesData) {
    var series = new TimeSeries({
      datapoints: seriesData.datapoints,
      alias: seriesData.target
    });

    series.flotpairs = series.getFlotPairs(this.panel.nullPointMode);
    return series;
  }

  onDataError(err) {
    this.onDataReceived([]);
  }

  link(scope, elem, attrs, ctrl) {
    elem = elem.find('.skycons-panel');

    function setElementHeight() {
      elem.css('height', ctrl.height + 'px');
    }

    function render(){
      setElementHeight();

      var icon = ctrl.data;
      if (!icon) {
          elem.html('');
          return;
      }

      var width = elem.width();
      var height = ctrl.height;

      // Compute the dimensions for the weather icon
      var dim;
      if (ctrl.iconWidth) {
         dim = ctrl.iconWidth;
      } else {
         dim = Math.floor(Math.min(width, height) * .75);
      }

      // Find the position for the canvas inside the <div>
      var canvasTop = Math.floor(height / 2 - dim / 2);
      var canvasLeft = Math.floor(width / 2 - dim / 2);

      var body = '';
      body += '<div width="' + width + '" height="' + height +'">';
      body += '  <canvas id="icon1" width="' + dim + '" height="' + dim + '" style="position: absolute; margin-top: ' + canvasTop + 'px; margin-left: ' + canvasLeft + 'px;"></canvas>';
      body += '  <script src="public/plugins/skycons-panel/scripts/skycons.js"></script>';
      body += '  <script>';
      body += '    var skycons = new Skycons({monochrome: false});';
      body += '    skycons.add("icon1", "' + icon + '");';
      body += '    skycons.play();';
      body += '  </script>';
      body += "</div>";

      elem.html(body);
    }

    this.events.on('render', function() {
      render();
      ctrl.renderingCompleted();
    });

  }

}

SkyconsCtrl.templateUrl = 'module.html';

